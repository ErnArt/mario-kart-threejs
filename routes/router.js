const express = require('express');
const router = express.Router();

const indexController = require('../controllers/indexController');
const circuitController = require('../controllers/circuitController');

router.get('/', indexController.index);
router.get('/circuitTimer', circuitController.circuitTimer);

module.exports = router;