# Mario Kart ThreeJS

## Description

This project is a ThreeJS test project for my third year of Undergraduate in IT studies. I did take inspiration of Mario Kart with ripped ressources from https://www.vg-resource.com to experiment and understand the framework. This is an evolution of my older Mario Kart ThreeJS project as a NodeJS and Express app.

This project is made up of :
- the framework (ThreeJS) used to render 3D scenes using WebGL.
- the framework (Express) for routing our application.
- the framework (EJS), Embedded JavaScript templates for better modularity.
- the Javascript library (Howler) for more functionalities for audio objects.
- the Javascript library (dat.gui) used to display a GUI on our main window.
- the daemon (nodemon) for refreshing the application when a change is made.
- the open-source 3D software (Blender) to convert .obj and .mtl to .glb.

## Dependencies

- npm

## Librairies installation

```sh
npm install
```

## Usage

```sh
npm run start
firefox localhost:3000
```

You can access the application directly on https://mariokart.ernart.com.

## Input

- Up : Going forward.
- Up + Shift key : Boost forward.
- Left : Left rotation.
- Down : Turning back.
- Right : Right rotation.
- F : First person camera.
- Space key : Resetting position.
- Mute button : To mute sound.