exports.circuitTimer = function(req, res)
{
	let characterVar = req.query.character;
	res.render('../views/layoutPage.ejs', { title: 'Circuit Timer', page : 'circuitTimer', character : characterVar});
}
